<?php
session_start();

/**
 * Override of default.config.php
 * Tutorial and options available here: https://github.com/xdan/jodit-connectors
 */
 
 


$config = [
	'sources' => [
		'images' => [
			'root' => $_SERVER["DOCUMENT_ROOT"] . '/development/jodit-pro/uploads',
			'baseurl' => 'http://localhost/development/jodit-pro/uploads',
			'createThumb' => false
		]
	]
];




/* Override Authentication: set read-only permission for all users, but give to users with the role - administrator full access: */
$config['accessControl'][] = array(
	'role'                => '*',

	'FILES'               => false,
	'FILE_MOVE'           => false,
	'FILE_UPLOAD'         => false,
	'FILE_UPLOAD_REMOTE'  => false,
	'FILE_REMOVE'         => false,
	'FILE_RENAME'         => false,

	'FOLDERS'             => false,
	'FOLDER_MOVE'         => false,
	'FOLDER_REMOVE'       => false,
	'FOLDER_RENAME'       => false,

	'IMAGE_RESIZE'        => false,
	'IMAGE_CROP'          => false,
);

$config['accessControl'][] = array(
	'role' => 'administrator',
	'FILES'               => true,
	'FILE_MOVE'           => true,
	'FILE_UPLOAD'         => true,
	'FILE_UPLOAD_REMOTE'  => true,
	'FILE_REMOVE'         => true,
	'FILE_RENAME'         => true,

	'FOLDERS'             => true,
	'FOLDER_MOVE'         => true,
	'FOLDER_REMOVE'       => true,
	'FOLDER_RENAME'       => true,

	'IMAGE_RESIZE'        => true,
	'IMAGE_CROP'          => true,
);




return $config;