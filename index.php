<?php 
session_start();
$_SESSION['JoditUserRole'] = 'administrator';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Jodit Pro Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/3.6.6/jodit.min.css"/>
	
</head>
<body>


<div class="container">
	<textarea id="editor" name="editor">Test value</textarea>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jodit/3.6.6/jodit.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#editor').each(function () {
		var editor = new Jodit(this, {
			license: '57C3E-3DJ43-ADNPL-U8O7Z',
			filebrowser: {
				ajax: {
					url: 'connector/index.php'
				}
			},
			uploader: {
				url: 'connector/index.php?action=fileUpload',
			}
		});
	});
});
	
</script>
</body>
</html>
